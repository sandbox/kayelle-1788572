<?php

/**
* @file
* A module for creating badges and associated files that comply with the Open Badges Infrastructure.
*/


/**
 * Constants.
 */

define('BASE_ADMIN_PATH', 'admin/config/user-interface/open-badge-it/settings');

//Local position
define('AWARD_EDIT_DELETE_ARG_POSITION', 5);

/**
 * Implements hook_help().
 */

function open_badge_it_help($path, $arg) {
	switch ($path) {
		case "admin/help#open_badge_it":
			return '<p>'.  t("Badges help - Coming Soon") .'</p>';
  }

}

/**
 * Implements hook_menu().
 */

function open_badge_it_menu() {
  $items = array();

  $items[BASE_ADMIN_PATH] = array(
    'title' => 'Open Badge-It',
		'description' => 'Configure Open Badge-It Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('open_badge_it_settings_edit_form'),
    //'access arguments' => array('manage badges'),
		'access arguments' => array('Administer Open Badges'),
    'file' => 'open_badge_it.admin.inc',
    );

		$items[BASE_ADMIN_PATH.'/settings'] = array(
    	'title' => 'Settings',
    	'type' => MENU_DEFAULT_LOCAL_TASK,
    	'description' => 'Configure Open Badge-It Settings settings',
    	'weight' => -10,
			);
			
	$items['obicallback/%/%/%'] = array(
    'title' => 'OBI Callback', 
    'description' => 'Receives response data from badge being issued to the obi', 
    'page callback' => 'update_award_status', 
    'page arguments' => array(1,2,3),
    'access arguments' => array('access content'), 
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implement hook_entity_info().
 *
**/

function open_badge_it_entity_info() {
  $return['open_badge'] = array(
    'label' => t('Open Badge'),
    'entity class' => 'OpenBadge',
    'controller class' => 'OpenBadgeController',
    'base table' => 'open_badge',
    'fieldable' => TRUE,
    'entity keys' => array(
        'id' => 'open_badge_id',
        'bundle' => 'type',
    ),
    'bundles' => array(),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'open_badge_uri',
    'creation callback' => 'open_badge_create',
    'access callback' => 'open_badge_access',
    'module' => 'open_badge_it',
    'admin ui' => array(
      'path' => 'admin/content/open-badge-it/open-badge',
      'file' => 'open_badge.admin.inc',
      'controller class' => 'OpenBadgeUIController',
      'menu wildcard' => '%open_badge',
    ),
		//'views controller class' => 'EntityDefaultViewsController',
  );

  $return['open_badge_type'] = array(
    'label' => t('Open Badge Type'),
    'entity class' => 'OpenBadgeType',
    'controller class' => 'OpenBadgeTypeController',
    'base table' => 'open_badge_type',
    'fieldable' => TRUE,
    'bundle of' => 'open_badge',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'open_badge_type_id',
      'name' => 'type',
      'label' => 'label',
    ),
    'access callback' => 'open_badge_type_access',
    'module' => 'open_badge_it',
    'admin ui' => array(
      'path' => 'admin/structure/open-badge-types',
      'file' => 'open_badge_type.admin.inc',
      'controller class' => 'OpenBadgeTypeUIController',
			'menu wildcard' => '%open_badge_type',
    ),
  );

	$return['open_badge_award'] = array(
    'label' => t('Open Badge Award'),
    'entity class' => 'OpenBadgeAward',
    'controller class' => 'OpenBadgeAwardController',
    'base table' => 'open_badge_award',
		'label callback' => 'open_badge_award_label',
    'fieldable' => FALSE,
		'bundles' => array(),
		'entity keys' => array(
      'id' => 'obiuid', 
    ),
    'access callback' => 'open_badge_award_access',
    'module' => 'open_badge_it',
    'admin ui' => array(
      'path' => 'admin/structure/open-badge-awards',
      'file' => 'open_badge_award.admin.inc',
      'controller class' => 'OpenBadgeAwardUIController',
    ),
  );

  return $return;
}

/**
 * Implements hook_entity_info_alter().
 *
 * We are adding the info about the open badge types via a hook to avoid a recursion
 * issue as loading the open badge types requires the entity info as well.
 *
 */
function open_badge_it_entity_info_alter(&$entity_info) {
  foreach (open_badge_get_types() as $type => $info) {

    $entity_info['open_badge']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/open-badge-types/manage/%open_badge_type',
        'real path' => 'admin/structure/open-badge-types/manage/' . $type,
        'bundle argument' => 4,
        'access arguments' => array('administer open badge types'),
      ),
    );
  }

}

/**
 * Implements hook_permission().
 */
function open_badge_it_permission() {

  $permissions = array(
    'administer open badge types' => array(
      'title' => t('Administer Open Badge types'),
      'description' => t('Create and delete fields for Open Badge types, and set their permissions.'),
    ),
    'administer open badges' => array(
      'title' => t('Administer Open Badges'),
      'description' => t('Edit and delete all open badges'),
    ),  

    'view open badges' => array(
      'title' => t('View Open Badge Awards'),
      'description' => t('view all open badge awards'),
    ), 
  );
  
  //Generate permissions per open badge 
  foreach (open_badge_get_types() as $type) {
    $type_name = check_plain($type->type);

    $permissions += array(
      "edit any $type_name open badge" => array(
        'title' => t('%type_name: Edit any open badge', array('%type_name' => $type->label)),
      ),
      "view any $type_name open badge" => array(
        'title' => t('%type_name: View any open badge', array('%type_name' => $type->label)),
      ),
    );
  }
  return $permissions;  
}

/**
 * Determines whether the given user has access to an open badge.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 * @param $openBadge
 *   Optionally an open badge or an open badge type to check access for. If nothing is
 *   given, access for all open badges is determined.
 * @param $account
 *   The user to check for. Leave it to NULL to check for the global user.
 * @return boolean
 *   Whether access is allowed or not.
 */
function open_badge_access($op, $openBadge = NULL, $account = NULL) {
  if (user_access('administer open badges', $account)) {
    return TRUE;
  }

  if (isset($openBadge) && $type_name = $openBadge->type) {
    $op = ($op == 'view') ? 'view' : 'edit';
    if (user_access("$op any $type_name open badge", $account)) {
      return TRUE;
    }
  }

  return FALSE;
}

function open_badge_award_access($op, $openBadgeAward = NULL, $account = NULL) {
  if (user_access('administer open badge awards', $account)) {
    return TRUE;
  }

  if (isset($openBadgeAward)) {
    $op = ($op == 'view') ? 'view' : 'edit';
    if (user_access("$op any open badge award", $account)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Access callback for the entity API.
 */
function open_badge_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer open badge types', $account);
}


/**
 * Gets an array of all open badge types, keyed by the type name.
 *
 * @param $type_name
 *   If set, the type with the given name is returned.
 * @return OpenBadgeType[]
 *   Depending whether $type isset, an array of open badge types or a single one.
 */
function open_badge_get_types($type_name = NULL) {

  // entity_load will get the Entity controller for our open badge entity and call the load
  // function of that object - we are loading entities by name here.

  $types = entity_load_multiple_by_name('open_badge_type', isset($type_name) ? array($type_name) : FALSE);

  return isset($type_name) ? reset($types) : $types;
}

/**
 * Menu argument loader; Load an open badge type by string.
 *
 * @param $type
 *   The machine-readable name of an open badge type to load.
 * @return
 *   An open badge type array or FALSE if $type does not exist.
 */
function open_badge_type_load($type) {
  return open_badge_get_types($type);
}

/**
 * Fetch an open badge object. Make sure that the wildcard you choose 
 * in the open badge entity definition fits the function name here.
 *
 * @param $open_badge_id
 *   Integer specifying the open badge id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   A fully-loaded $openBadge object or FALSE if it cannot be loaded.
 *
 * @see open_badge_load_multiple()
 */
function open_badge_load($open_badge_id, $reset = FALSE) {
  $openBadges = open_badge_load_multiple(array($open_badge_id), array(), $reset);
  return reset($openBadges);
}

/**
 * Load multiple open badges based on certain conditions.
 *
 * @param $open_badges_ids
 *   An array of open badge IDs.
 * @param $conditions
 *   An array of conditions to match against the {open_badge} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of open_badge objects, indexed by open_badge_id.
 *
 * @see entity_load()
 * @see open_badge_load()
 */
function open_badge_load_multiple($open_badge_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('open_badge', $open_badge_ids, $conditions, $reset);
}

/**
 * Deletes an open badge.
 */
function open_badge_delete(OpenBadge $open_badge) {
  $open_badge->delete();
}


/**
 * Delete multiple open_badges.
 *
 * @param $open_badge_ids
 *   An array of open badge IDs.
 */
function open_badge_delete_multiple(array $open_badge_ids) {
  entity_get_controller('open_badge')->delete($open_badge_ids);
}

/**
 * Create an open badge object.
 */
function open_badge_create($values = array()) {
  return entity_get_controller('open_badge')->create($values);
}

/**
 * Create an open badge award object.
 */

function open_badge_award_create($values = array()) {
  return entity_get_controller('open_badge_award')->create($values);
}


function open_badge_award_wrapper_load(OpenBadgeAward $open_badge_award) {	
	return entity_metadata_wrapper('open_badge_award', $open_badge_award);
}

function open_badge_award_user_load(OpenBadgeAward $open_badge_award) {
	
	$uid = open_badge_award_wrapper_load($open_badge_award)->uid->value();
	$user = isset($uid) ? user_load($uid) : array();
	
	return $user;
}

/**
 * This function displays the username and badge as the label.
 * Will redo the overview page.
 * @todo This causes errors on the add open badge award form
 */

function open_badge_award_label(OpenBadgeAward $open_badge_award) {
	
	$award = open_badge_award_wrapper_load($open_badge_award);	
	$badge_id = $award->open_badge_id->value();
	$uid = $award->uid->value();
	
	if (isset($badge_id)) {
		
		$user_name = user_load($uid)->name;
 		$badge_name = open_badge_load($badge_id)->name;
		return $user_name . ' - ' . $badge_name;
	}
	else {
		return;
	}
}


/**
 * URI callback for badges
 */

function open_badge_uri(OpenBadge $openBadge){
  return array(
    'path' => 'open-badge/' . $openBadge->open_badge_id . '/view',
  );
}


/**
 * Menu title callback for showing individual entities
 */

function open_badge_page_title(OpenBadge $openBadge){
  return $openBadge->name;
}

/**
 * Sets up content to show an individual badge
 * @todo - get rid of drupal_set_title(); ?
 */

function open_badge_page_view($openBadge, $view_mode = 'full') {    
  $controller = entity_get_controller('open_badge');  
  $content = $controller->view(array($openBadge->open_badge_id => $openBadge));
drupal_set_title(''); //hack - did this to hide plain text title. Fix this asap.
  return $content;
}



/**
 * Implements hook_views_api().
 */
function open_badge_it_views_api() {
  return array(
    'api' => 3.0,
		'path' => drupal_get_path('module', 'open_badge_it') . '/views',
  );
}


/**
 * Implement hook_theme().
 */

function open_badge_it_theme() {
  return array(
    'open_badge_add_list' => array(
      'variables' => array('content' => array()),
      'file' => 'open_badge.admin.inc',
    ),
    'open_badge' => array(
      'render element' => 'elements',
      'template' => 'openbadge',
    ),
    'open_badge_admin' => array(
      'render element' => 'elements',
      'template' => 'open_badge_admin',
    ),
  );
}



/**
 * Implements hook_menu_local_tasks_alter().
 */
function open_badge_it_menu_local_tasks_alter(&$data, $router_item, $root_path) {

  if ($root_path == 'admin/content/open-badge-it/open-badge') {
    $item = menu_get_item('admin/content/open-badge-it/open-badge/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }

}



/**
 * The class used for open badge entities
 */
class OpenBadge extends Entity {
  
  public function __construct($values = array()) {
    parent::__construct($values, 'open_badge');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'badge/' . $this->open_badge_id);
  }
   
}

/**
 * The class used for open badge type entities
 */
class OpenBadgeType extends Entity {
  
  public $type;
  public $name;
  
  public function __construct($values = array()) {
    parent::__construct($values, 'open_badge_type');
  }
  
}

class OpenBadgeAward extends Entity {
	
	//public $type;
  
  public function __construct($values = array()) {
    parent::__construct($values, 'open_badge_award');
  }

}


/**
 * The Controller for OpenBadge entities
 */
class OpenBadgeController extends EntityAPIController {
	
  public function __construct($entityType) {
    parent::__construct($entityType);
  }


  /**
   * Create a badge - we first set up the values that are specific
   * to our open badge-it schema but then also go through the EntityAPIController
   * function.
   * 
   * @param $type
   *   The machine-readable type of the open badge.
   *
   * @return
   *   An open badge object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Open Badge
    $values += array( 
      'open_badge_id' => '',
      'is_new' => TRUE,
      'title' => '',
      'created' => '',
      'changed' => '',
      'data' => '',
    );

    $openBadge = parent::create($values);
    return $openBadge;
  }

}

/**
 * The Controller for Open Badge Type entities
 */
class OpenBadgeTypeController extends EntityAPIControllerExportable {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }
  
   /**
   * Create an open badge type - we first set up the values that are specific
   * to our open badge type schema but then also go through the EntityAPIController
   * function.
   * 
   * @param $type
   *   The machine-readable type of the open badge type.
   *
   * @return
   *   A open badge type object with all default fields initialized.
   */
  public function create(array $values = array()) {

    $values += array( 
      'open_badge_type_id' => '',
      'is_new' => TRUE,
			'module' => 'open_badge_it',
      'data' => '',
    );
    $open_badge_type = parent::create($values);
    return $open_badge_type;
  }

}

/**
 * The Controller for Open Badge Award entities
 */

class OpenBadgeAwardController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }
  
   /**
   * Create an open badge award - we first set up the values that are specific
   * 
   * @param $type
   *   The machine-readable type of the open badge award.
   *
   * @return
   *   A open badge type object with all default fields initialized.
   */
 public function create(array $values = array()) {

    $values += array( 
      'obiuid' => '',
      'is_new' => TRUE,
			'award_date' => time(),
			'status' => 'awarded',	
    );
    $open_badge_award = parent::create($values);
    return $open_badge_award;
  }

}


/**
 * Creates json assertions for awarded badges.
 */

function open_badge_assertion_create($open_badge_award, $unlink = FALSE) {
	
		//Award info;
		
		$award = entity_metadata_wrapper('open_badge_award', $open_badge_award);
		
		$obiuid = $award->obiuid->value();
		$evidence_url = $award->evidence_url->value();
		$issued_date = ($award->award_issued_date->value() > 0 ) ? date('Y-m-d',$award->award_issued_date->value()) : '';
		$expiration_date = ($award->expiration_date->value() > 0 ) ? date('Y-m-d',$award->expiration_date->value()) : '';
		$issuer_name = variable_get('open_badge_it_issuer_name');
		$issuer_org	= variable_get('open_badge_it_issuer_org');
		$issuer_contact_email	= variable_get('open_badge_it_issuer_contact_email');
		
		// User info;
		
		$user_email = user_load($award->uid->value())->mail;
		
		// Salt email	
		$salt = rand_string(8); //randomized everytime
		$hashed_email = hash('sha256', $user_email  . $salt);		
		
		// Badge info;
		$open_badge_id = $award->open_badge_id->value();
		$badge = open_badge_load($open_badge_id);	
		$badge_name = $badge->name;
		$badge_type = $badge->type;
		
		$image_field = "field_".$badge_type."_image";
		$badge_image_path = file_uri_target($badge->{$image_field}['und'][0]['uri']);
		$badge_image = "/sites/default/files/" . $badge_image_path;
		
		$desc_field = "field_".$badge_type."_desc";
		$badge_desc = $badge->{$desc_field}['und'][0]['safe_value'];
		$badge_criteria_url = $GLOBALS['base_root']."/open-badge/".$open_badge_id;
		
		$version_field = "field_".$badge_type."_version";
		$badge_version = $badge->{$version_field}['und'][0]['safe_value'];
			
		
		$json_file_path = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['base_path'] . "sites/default/files/" . variable_get('open_badge_it_json_dir')  . "/" . $obiuid   . '.json';
		
		// Delete file if this is an update.
		if ($unlink == TRUE) {
			unlink($json_file_path);
		}
		
		$handle = fopen($json_file_path, 'c');
		
		$assertion = array(
			'recipient' => "sha256$".$hashed_email,
			'salt' => $salt,
			'evidence' => $evidence_url,
			'badge' => array(
				'version' => $badge_version,
				'name' => $badge_name,
				'image' => $badge_image,
				'description' => $badge_desc,
				'criteria' => $badge_criteria_url,
				'issued_on'=> $issued_date,
				'expires' => $expiration_date,
				'issuer' => array(
				'origin' => $GLOBALS['base_root'],
				'name' => $issuer_name,
				'org' => $issuer_org,
				'contact' => $issuer_contact_email,
				),
			),
		);
		
	fwrite($handle, json_encode($assertion));
	fclose($handle);
			
	return;

}

function rand_string( $length ) { 
	
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$str = '';
	$size = strlen( $chars );
	for( $i = 0; $i < $length; $i++ ) {
		$str .= $chars[ rand( 0, $size - 1 ) ];
	}
	
	return $str;
}

/**
 * Deletes json assertions for awarded badges.
 */

function open_badge_assertion_delete($open_badge_award) {	
	
	$award = entity_metadata_wrapper('open_badge_award', $open_badge_award);
	$obiuid = $award->obiuid->value();
	
	$json_file_path = $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['base_path'] . "sites/default/files/" . variable_get('open_badge_it_json_dir')  . "/" . $obiuid   . '.json';
	
	unlink($json_file_path);
	
	$open_badge_award->delete();
		
	return;
		
}



/**
 * Adds functionality for users to issue their badges to the obi
 */

function open_badge_it_page_build(&$page) {
	if (arg(0) == "open-badge-awards" || "profile-student") {
	
	// Only need to load the api url on the user's award page. Add JS file here too.

  	global $user;

		if ($user->uid > 0 ) {
    	drupal_add_js(variable_get('open_badge_it_api_url'), 'external');
			drupal_add_js(url(drupal_get_path('module', 'open_badge_it')) . '/open_badge_it.js');
  	}
	}
}


/**
 * Updates the status of the awarded badge once it has been issued to the obi.
 */


function update_award_status($state, $obiuid, $msg) {
	
	if ($state == "error") {		
		$award_updated =	db_update('open_badge_award')->fields(array('status_message' => $msg,'obi_issued_date' => time(),))->condition('obiuid', $obiuid, '=')->execute();
	}
	
	if ($state == "success" ) {
		$award_updated =	db_update('open_badge_award')->fields(array('status' => "issued", 'obi_issued_date' => time(),))->condition('obiuid', $obiuid, '=')->execute();
	}

	cache_clear_all();
	
	if ($award_updated) {
	}
	
	return $ajax_status;

}

