// Javascript that does the OBI issuing. The callback refers to a function in the module file called: update_award_status
// which changes the open_badge_award status from "awarded" to "issued".

(function ($) {
		
	Drupal.behaviors.obiIssue = {
		attach: function(context, settings) { 
			
			$('[class^=backpack-button]').bind('click', function() {
				
						var $this = $(this);
						var obiuid = $this.attr('id');

						var assertionUrl = Drupal.settings.baseRoot + Drupal.settings.basePath + 'sites/default/files/' + Drupal.settings.assertion_dir + '/' + obiuid + '.json';
	
				OpenBadges.issue([''+assertionUrl+''], function(errors, successes) { 
					
					var callBackURL = Drupal.settings.baseRoot + '/obicallback';

					if (errors.length > 0 ) {
						$.ajax({
							url: 'http://www.hubprov.com/obicallback/error/' + obiuid + '/' + errors.toSource(),
							type: 'GET',
						});
					}
					
					if (successes.length > 0) {
					$this.hide();
						$.ajax({
							url: 'http://www.hubprov.com/obicallback/success/' + obiuid + '/' + errors.toSource(),
							type: 'GET',
						});
					}
				});	
				
				return false;
		});    
	
	}
	
	};
	
})(jQuery);