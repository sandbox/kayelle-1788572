<?php

/**
 * @file
 * Open Badge type editing UI.
 */

class OpenBadgeAwardUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {

    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Award and manage badges for users.';
		
		$id_count = count(explode('/', $this->path));
		
		$items[$this->path . '/manage/%/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'open_badge_award_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'open_badge_award_access',
      'access arguments' => array('edit', $id_count + 1),
			'load arguments' => array($id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'open_badge_award.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
		
    return $items;
  }

}

/**
 * Retrieve open badges by type
 */

function retrieve_badges_by_type($type){
	
	$query = new EntityFieldQuery;

	$query->entityCondition('entity_type', 'open_badge')
  	->entityCondition('bundle', $type);

	$results = $query->execute();
	
	if (isset($results['open_badge'])) {
  	$open_badges = entity_load('open_badge', array_keys($results['open_badge']));
		
  	foreach($open_badges as $badge){
			$badges_list[$badge->open_badge_id] = $badge->name;
		//	$badges_list[$badge->name] = $badge->name;
		}
	}

	asort($badges_list);	
	
	return $badges_list;
		
}

/**
 * Retrieve users list by badge type roles
 */

function retrieve_badge_roles_users($badge_types_arr, $badge_type) {
	
	foreach($badge_types_arr as $type){
		$badge_roles[$type->type] = unserialize($type->roles);
	}
	
	$type_roles = $badge_roles[$badge_type];
	
	//Exclude Admin
	foreach ($type_roles as $key => $value) {
		if ( $value == 0) {
			unset($type_roles[$key]);
		}
	}
	
	$users = entity_load('user');
	asort($users);
	
	foreach ($users as $user) {
		$roles = array_intersect_key($type_roles,$user->roles);
		if (count($roles) > 0) {
		$badgeable_users[$user->uid] = $user->name;
		}	
	}
	

	asort($badgeable_users);
	
	return $badgeable_users;	
	
}



/**
 * Generates the open badge award form.
 *
**/

function open_badge_award_form($form, &$form_state, $open_badge_award, $op = 'edit') {
	
	$form_state['op'] = $op;
	
	// Warning to set images and assertion directories in config;

	$images_dir = variable_get('open_badge_it_images_dir');
	$json_dir = variable_get('open_badge_it_images_dir');
	
	if (empty($images_dir) || empty($json_dir)) {
		  drupal_set_message(t('Edit the !url for Open Badge-It before adding an Open Badge Type.', array('!url' => l(t('configuration settings'), 'admin/config/user-interface/open-badge-it/settings'))), 'error');
	}
	
	// Load the award entity metadata.
		
	$award = entity_metadata_wrapper('open_badge_award', $open_badge_award);
	
	switch ($op) {
		
		case 'delete':

			return drupal_get_form('open_badge_award_delete_form', $open_badge_award);
		
		// Edit existing award.
		case 'edit':
	
	// No reassignment of badge & user. Can delete but not reassign.
		
			$open_badge_id = $award->open_badge_id->value();
		
			$badge_name = open_badge_load($open_badge_id)->name;	
			$user_name = open_badge_award_user_load($open_badge_award)->name;
			$form_state['obiuid'] = $open_badge_id;
		
			$form['#prefix']	= "<h4>Edit the " . $badge_name . " open badge awarded to " . $user_name . ".</h4><div><em>Note: Changes will also modify the badge assertion file for the OBI.</em></div>";
		
			$form['submit'] = array(
    		'#type' => 'submit',
				'#weight'	=> 7,
    		'#value' =>  t('Update'),
			);

		break;
		
		// New award.
		case 'add':
		
		$form['submit'] = array(
			'#type' => 'submit',
			'#weight'	=> 7,
			'#value' =>  t('Award'),
		);
	
	// Get Badge Types.
	
	$badge_types_arr = entity_load('open_badge_type');
	
	asort($badge_types_arr);
	
	foreach($badge_types_arr as $type){
		$badge_type_list[] = $type->type;
	}
			
		$form['badge_type'] = array(
    	'#title' => t("Select a Badge Type"),
    	'#type' => 'select',
    	'#options' => $badge_type_list,
			'#default_value' => $badge_type_list[0],
			'#weight'	=> 1,
    	'#ajax' => array(
      	'callback' => 'open_badge_award_form_callback',
				'method'	=> 'replace',
      	'wrapper' => 'badge-list',
			),
  	);

		$form['#prefix']	= '<div id="badge-list">';

		// Badge List for type selected;
		
		$form['open_badge_id'] = array(
			'#type' => 'select',
			'#title' => t('Choose badge'),
			'#options' => retrieve_badges_by_type($badge_type_list[0]),
			'#default_value' => '',
			'#weight'	=> 2,
		);
	
	// Select users with roles for the badge type.
	
	$form['uid'] = array(
		'#type' => 'select',
		'#title' => t('Award badge to'),
		'#options' => retrieve_badge_roles_users($badge_types_arr,$badge_type_list[0]),
		'#default_value' => '',
		'#weight'	=> 3,
	);
	
	$form['#suffix']	=	'</div>';	
	
		if (!empty($form_state['values']['badge_type'])) {
		$form['open_badge_id']['#options'] = retrieve_badges_by_type($badge_type_list[$form_state['values']['badge_type']]);
		$form['uid']['#options'] = retrieve_badge_roles_users($badge_types_arr,$badge_type_list[$form_state['values']['badge_type']]);
	}
	
		break;
	}
	

	// Evidence URL;
	$form['evidence_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Evidence URL'),
		'#description'	=> t('User-specific URL with information about this specific badge instance. Example: '. $GLOBALS['base_root']),
		'#size' => 60, 
		'#maxlength' => 128,
		'#weight'	=> 4, 
		'#default_value' => isset($award->evidence_url) ? $award->evidence_url->value() : '',
	);


	$date_format = 'Y-m-d';

	$expiration_date = $award->expiration_date->value();
	$award_issued_date = $award->award_issued_date->value();
		
	// Expiration Date;
	$form['badge_expires'] = array(
		'#type' => 'date_popup', 
		'#title' => t('Badge Expiration Date'), 
		'#description'	=> t('Date when the badge expires. If omitted, the badge never expires.'),
		'#date_format'	=> $date_format,
		'#weight'	=> 5,
		'#default_value' => isset($expiration_date) ? date('Y-m-d', $expiration_date) : null,
	);
	
		// Issued Date;
	$form['badge_issued'] = array(
		'#type' => 'date_popup', 
		'#title' => t('Badge Issued Date'), 
		'#description'	=> t('Date when the badge is issued to the OBI. If omitted, the issue date will be set to whenever the OBI receives the badge.'),
		'#date_format'	=> $date_format,
		'#weight'	=> 6,
		'#default_value' => isset($award_issued_date) ? date('Y-m-d', $award_issued_date) : null,
	);

	return $form;
}

/**
 * AJAX callback for the badge type, badge and user select drop downs;
 */

function open_badge_award_form_callback($form, $form_state) {

	return $form;
}

/**
 * Form API validate callback;
 * Validation doesn't pertain to edits. Only validating for duplicate entries.
 * @todo Add more validation
 */

function open_badge_award_form_validate(&$form, &$form_state) {
	
	// @todo This can be better.
	// @tod add file directory validation
	
	$op = $form_state['clicked_button']['#value'];
	
	// Do a littl url checking. The OBI wants evidence urls to contain the entire directive (for right now). Make this better.

		if (!empty($form_state['values']['evidence_url']) && ! preg_match('/(((f|ht){1}(tp|tps):\/\/)[\w\d\S]+)/i', $form_state['values']['evidence_url'])) {
			form_set_error('evidence_url', t('Please check that your Evidence Url is a valid web site. It should begin with "http://" or "https://"'));
		} 

	switch ($op) {
		case t('Award'):			
			$check_award_exists = db_query("SELECT uid FROM {open_badge_award} WHERE uid = :uid and open_badge_id = :open_badge_id", 
			array(':uid' => $form_state['values']['uid'], ':open_badge_id' => $form_state['values']['open_badge_id']));
	
			if ($check_award_exists->rowCount() > 0) {
				form_set_error('uid', t('This badge has already been assigned to this user.'));
			}	
		break;
	}	

}

/**
 * Form API submit callback for the type form.
 */

function open_badge_award_form_submit($form, &$form_state) {
	
	$op = $form_state['clicked_button']['#value'];
	
	$dates = award_dates_prep($form_state);
	
	foreach ($dates as $key => $val) {
		$form_state['values'][$key] = $val;	
	}
		
  $badge_award = entity_ui_form_submit_build_entity($form, $form_state);
  $badge_award->save();
	$badge_name = entity_metadata_wrapper('open_badge', $badge_award->open_badge_id)->name->value();
	$user_name = user_load($badge_award->uid)->name;
	
	// Return status message.
	
	switch ($op) {
    	case t('Update'):	
				// ReCreate the badge assertion.
				open_badge_assertion_create($badge_award, TRUE);	
				drupal_set_message(t('The open badge %badgename award for %username has been updated.', array('%badgename' => $badge_name, '%username' => $user_name)));	
			break;
			case t('Award'):
				// Create the badge assertion.
				open_badge_assertion_create($badge_award);
				drupal_set_message(t('The %badgename open badge has been awarded to %username.', array('%badgename' => $badge_name, '%username' => $user_name)));	
			break;
		}
	
  $form_state['redirect'] = 'admin/structure/open-badge-awards';
}

function open_badge_award_delete_form_wrapper($obiuid) {
	$open_badge_award = entity_load('open_badge_award',array($obiuid));
  return drupal_get_form('open_badge_award_delete_form', $open_badge_award,$obiuid);
}


function open_badge_award_delete_form($form, &$form_state, $open_badge_award, $obiuid) {
	
	$form_state['open_badge_award'] = $open_badge_award[$obiuid];
  
  $form['#submit'][] = 'open_badge_award_delete_form_submit';
	
	$badge_award = entity_metadata_wrapper('open_badge_award', $open_badge_award[$obiuid]);
	$badge_name = entity_metadata_wrapper('open_badge', $badge_award->open_badge_id->value())->name->value();
	$user_name = user_load($badge_award->uid->value())->name;

  $form = confirm_form($form,
    t('Are you sure you want to delete The open badge %badgename award for %username?', array('%badgename' => $badge_name, '%username' => $user_name)),
    'admin/structure/open-badge-awards',
    t('If %username has issued this badge to the OBI, deleting the badge will invalidate it in the backpack. This action cannot be undone.', array('%username' => $user_name)),
    t('Delete'),t('Cancel'),'confirm'
  );
  
  return $form;
}


function open_badge_award_delete_form_submit($form, &$form_state) {
	
	$badge_award = $form_state['open_badge_award'];

	//Delete assertion 
	open_badge_assertion_delete($badge_award);
	
	$badge_award->delete();

	$badge_name = entity_metadata_wrapper('open_badge', $badge_award->open_badge_id)->name->value();
	$user_name = user_load($badge_award->uid)->name;
	
	$badge_username = array('%badgename' => $badge_name, '%username' => $user_name);

  drupal_set_message(t('The open badge %badgename award for %username has been deleted.', $badge_username));
  watchdog('open_badge_it', 'Deleted Open Badge %badgename award for %username.', $badge_username);

  $form_state['redirect'] = 'admin/structure/open-badge-awards';
}


/**
 * Clean up variables for submit and edit
 */

function award_dates_prep(&$form_state) {
	
	// Convert dates to unix timestamps.
	$dates['expiration_date'] = isset($form_state['values']['badge_expires']) ? strtotime($form_state['values']['badge_expires']) : null;
	$dates['award_issued_date'] = isset($form_state['values']['badge_issued']) ? strtotime($form_state['values']['badge_issued']) : null;

	return $dates;

}
