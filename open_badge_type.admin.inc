<?php

/**
 * @file
 * Open Badge type editing UI.
 */

class OpenBadgeTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage open badge entity types, including adding
		and removing fields and the display of fields.';
		
		$id_count = count(explode('/', $this->path));

    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

	$items[$this->path . '/manage/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'open_badge_type_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'open_badge_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'open_badge_type.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    return $items;
  }
}


/**
 * Form callback wrapper: delete an open badge.
 *
 * @param $openBadge
 *   The open badge object being edited by this form.
 *
 * @see open_badge_edit_form()
 */
function open_badge_type_delete_form_wrapper($open_badge_type) {
  // Add the breadcrumb for the form's location.
//	open_badge_set_breadcrumb();
	
  return drupal_get_form('open_badge_type_delete_form', $open_badge_type);
}

/**
 * Generates the open badge type editing form.
 * @todo add delete button
 */
function open_badge_type_form($form, &$form_state, $open_badge_type, $op = 'edit') {
	
	$images_dir = variable_get('open_badge_it_images_dir');
	$json_dir = variable_get('open_badge_it_images_dir');
	
	if (empty($images_dir) || empty($json_dir)) {
		  drupal_set_message(t('Edit the !url for Open Badge-It before adding an Open Badge Type.', array('!url' => l(t('configuration settings'), 'admin/config/user-interface/open-badge-it/settings'))), 'error');
	}

  if ($op == 'clone') {
    $open_badge_type->label .= ' (cloned)';
    $open_badge_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($open_badge_type->label) ? $open_badge_type->label : '',
    '#description' => t('The human-readable label of this open badge type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($open_badge_type->type) ? $open_badge_type->type : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'open_badge_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this open badge type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['issuable'] = array(
    '#type' => 'checkbox',
    '#title' => t('This badge is issuable to the OBI'),
    '#default_value' => isset($open_badge_type->issuable) && $open_badge_type->issuable == 1 ? TRUE : '',
  );
	
	$form['roles'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Role(s) that may be awarded this badge type'),
		'#options' => user_roles(TRUE),
		'#required' => TRUE,
		'#default_value' 	=> isset($open_badge_type->roles) ? unserialize($open_badge_type->roles) : array(),
	);


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save open badge type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API validate callback for the type form.
 */

function open_badge_type_form_validate(&$form, &$form_state) {
	
	// Really need to set the directories before proceeding.
	
	$images_dir = variable_get('open_badge_it_images_dir');
	$json_dir = variable_get('open_badge_it_images_dir');
	
	if (empty($images_dir) || empty($json_dir)) {
		  drupal_set_message(t('Edit the !url for Open Badge-It before adding an Open Badge Type.', array('!url' => l(t('configuration settings'), 'admin/config/user-interface/open-badge-it/settings'))), 'error');
	}
	
}

/**
 * Form API submit callback for the type form.
 */
function open_badge_type_form_submit(&$form, &$form_state) {
	$form_state['values']['roles'] = serialize($form_state['values']['roles']);
  $open_badge_type = entity_ui_form_submit_build_entity($form, $form_state);
  $open_badge_type->save();
	
	// Add fields to the open badge entity for this open badge type
	
	foreach (open_badge_it_installed_fields($open_badge_type->type) as $field) {
			
			$exists = field_read_field($field['field_name']);
			
			if (empty($exists)) {
    		field_create_field($field);
			}
  }

  foreach (open_badge_it_installed_instances($open_badge_type->type) as $instance) {

		$exists = field_read_instance('open_badge', $instance['field_name'], $open_badge_type->type);
		if (empty($exists)) {
    	$instance['entity_type'] = 'open_badge';
    	$instance['bundle'] = $open_badge_type->type;
    	field_create_instance($instance);
		}
  }

  $form_state['redirect'] = 'admin/structure/open-badge-types';
}

/**
 * Form API submit callback for the delete button.
 */

function open_badge_type_form_submit_delete(&$form, &$form_state) {	
  $form_state['redirect'] = 'admin/structure/open-badge-types/manage/' . $form_state['open_badge_type']->type . '/delete';
}

/**
 * Form callback: confirmation form for deleting an open badge.
 *
 * @param $openBadge
 *   The open badge to delete
 *
 * @see confirm_form()
 */
function open_badge_type_delete_form($form, &$form_state, $open_badge_type) {
  $form_state['open_badge'] = $open_badge_type;

  $form['#submit'][] = 'open_badge_type_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete Open Badge type %type?', array('%type' => $open_badge_type->type)),
    'admin/structure/open-badge-types/manage',
    t('Any associated Open Badges will also be deleted. If Open Badges have been issued to the OBI, deleting them will invalidate them in the user\'s backpack. This action cannot be undone.'),
    t('Delete'),t('Cancel'),'confirm'
  );
  
  return $form;
}

/**
 * Submit callback for open_badge_delete_form
 */
function open_badge_type_delete_form_submit($form, &$form_state) {
  $open_badge_type = $form_state['open_badge'];

	$open_badge_type->delete();
	
	// Delete Associated Open Badges
	
	$query = new EntityFieldQuery;

	$query->entityCondition('entity_type', 'open_badge')
  	->entityCondition('bundle', $open_badge_type->type);

	$results = $query->execute();
	
	if (isset($results['open_badge'])) {
  	$open_badges = entity_load('open_badge', array_keys($results['open_badge']));
		
  	foreach($open_badges as $badge){
			open_badge_delete($badge);
		}
	}
	
	//delete files?

  drupal_set_message(t('The Open Badge type %type has been deleted.', array('%type' => $open_badge_type->type)));
  watchdog('open_badge_it', 'Deleted Open Badge type %type.', array('%type' => $open_badge_type->type));

  $form_state['redirect'] = 'admin/structure/open-badge-types/manage';
}

/** Fields for Open Badge **/

function open_badge_it_installed_fields($type) {
  return array(
   'badge_image' 		=> array(
      'field_name' 	=> 'field_'.$type.'_image',
			'type'        => 'image',
      'cardinality' => 1,
			'required' 		=> TRUE,
    ),

   'badge_description' 		=> array(
      'field_name' 	=> 'field_'.$type.'_desc',
			'type'        => 'text',
      'cardinality' => 1,
			'required' 		=> TRUE,
    ),	

   'badge_criteria' 		=> array(
      'field_name' 	=> 'field_'.$type.'_criteria',
			'type'        => 'text_long',
			'module' => 'text',
      'cardinality' => 1,
			'required' 		=> FALSE,
    ),

   'badge_version' 		=> array(
      'field_name' 	=> 'field_'.$type.'_version',
			'type'        => 'text',
      'cardinality' => 1,
			'required' 		=> TRUE,
			'default_value'	> "0.5.0",
    ),	
	);
}

/** Field instances for Open Badge **/

function open_badge_it_installed_instances($type) {

  return array(

    'badge_image' 	=> array(
			'field_name'    => 'field_'.$type.'_image',
			'label'         => t('Badge Image'),
			'description'   => t('This is the image that will be displayed for the badge on your website and in the Open Badges backpack. Images should be no larger than 256k. The image will be resized to 90 x 90 if necessary.'),
			'required'	=> 1,
			'widget'     		=> array(
				'type'     		=> 'image_image',
				'weight'				=> 3,
				'settings' 		=> array(
      		'progress_indicator' => 'throbber',
      		'preview_image_style' => 'thumbnail',
    		),
			),
			
			'settings' => array(
    		'file_directory' => variable_get('open_badge_it_images_dir'),
    		'file_extensions' => 'png',
    		'max_filesize' => '256KB',
    		'max_resolution' => '90x90',
    		'min_resolution' => '90x90',
			),			
		),
		
		'badge_description' 	=> array(
			'field_name'	=> 'field_'.$type.'_desc',
			'label' 			=> t('Badge Description'),
			'description'	=> t('Description of the badge being issued to the Open Badges backpack. Maximum of 128 characters.'),
			'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
        ),
			),
			'widget' 			=> array(
      	'active' 		=> 1,
      	'module' 		=> 'text',
				'weight'				=> 2,
        'settings'	=> array(
         'size' 		=> '128',
        ),
			),
			'type' 			=> 'text_textfield',
			'required'	=> 1,
		),
		
		'badge_criteria' 	=> array(
			'field_name'	=> 'field_'.$type.'_criteria',
			'label' 			=> t('Badge Criteria'),
			'widget' => array(
    		'weight' => '4',
    		'type' => 'text_textarea',
    		'module' => 'text',
    		'active' => 1,
    		'settings' => array(
      		'rows' => '5',
    		),
			),
			'settings' => array(
    		'text_processing' => '1',
    		'user_register_form' => FALSE,
  		),
  		'display' => array(
    		'default' => array(
      		'label' => 'above',
      		'type' => 'text_default',
      		'settings' => array(),
      		'module' => 'text',
      		'weight' => 2,
    		),
  		),
  		'required' => 1,
  		'description' => t('Criteria content for this badge. Consider using <a href="http://www.lrmi.net/" target="_blank">LRMI</a>.'),
  		'default_value' => NULL,
  		'deleted' => '0',
		),
		
		'badge_version' 	=> array(
			'field_name'	=> 'field_'.$type.'_version',
			'label' 			=> t('Badge Version'),
			'description'	=> t('The version of the badge.'),
			'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
        ),
			),
			'widget' 			=> array(
      	'active' 		=> 1,
      	'module' 		=> 'text',
				'weight'				=> 4,
        'settings'	=> array(
         'size' 		=> '10',
        ),
			),
			'type' 			=> 'text_textfield',
			'required'	=> 1,
			'default_value' => array(
    		array('value' => '0.5.0'),
			),
		),
  );
}

