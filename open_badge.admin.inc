<?php

/**
 * @file
 * Open Badge editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class OpenBadgeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    
    $items = array();
    $id_count = count(explode('/', $this->path));

    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Open Badges',
      'description' => 'Add edit and update Open Badges.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the overview menu type for the list of open badges.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    
    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add an Open Badge',
      'description' => 'Add a new Open Badge',
      'page callback'  => 'open_badge_add_page',
      'access callback'  => 'open_badge_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'open_badge.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])

    );
    
    // Add menu items to add each different type of entity.
    foreach (open_badge_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label . ' Open Badge',
        'page callback' => 'open_badge_form_wrapper',
        'page arguments' => array(open_badge_create(array('type' => $type->type))),
        'access callback' => 'open_badge_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'open_badge.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing Open Badge entities
    $items[$this->path . '/badge/' . $wildcard] = array(
      'page callback' => 'open_badge_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'open_badge_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'open_badge.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    $items[$this->path . '/badge/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    
    $items[$this->path . '/badge/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'open_badge_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'open_badge_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'open_badge.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    
    // Menu item for viewing open badges
    $items['open-badge/' . $wildcard] = array(
      'title' => 'open_badge_page_title',
      'title callback' => 'open_badge_page_title',
      'title arguments' => array(1),
      'page callback' => 'open_badge_page_view',
      'page arguments' => array(1),
      'access callback' => 'open_badge_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );

    return $items;

  }
  
  
  /**
   * Create the markup for the add open badge Entities page within the class
   * so it can easily be extended/overriden.
   */ 
  public function addPage() {
    $item = menu_get_item();

    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }    
        
    return theme('open_badge_add_list', array('content' => $content));
  }
  
}


/**
 * Form callback wrapper: create or edit an open badge.
 *
 * @param $openBadge
 *   The open badge object being edited by this form.
 *
 * @see open_badge_edit_form()
 */
function open_badge_form_wrapper($openBadge) {
  // Add the breadcrumb for the form's location.
  open_badge_set_breadcrumb();

  return drupal_get_form('open_badge_edit_form', $openBadge);
}


/**
 * Form callback wrapper: delete an open badge.
 *
 * @param $openBadge
 *   The open badge object being edited by this form.
 *
 * @see open_badge_edit_form()
 */
function open_badge_delete_form_wrapper($openBadge) {
  // Add the breadcrumb for the form's location.
	open_badge_set_breadcrumb();
	
  return drupal_get_form('open_badge_delete_form', $openBadge);
}

/**
 * Form callback: create or edit an open badge.
 *
 * @param $openBadge
 *   The open badge object to edit or for a create form an empty open badge object
 *     with only an open badge type defined.
 */
function open_badge_edit_form($form, &$form_state, $openBadge) {
	
  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Badge Name'),
    '#default_value' => isset($openBadge->name) ? $openBadge->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );


  // Add the field related form elements.
  $form_state['open_badge'] = $openBadge;
  field_attach_form('open_badge', $openBadge, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save open badge'),
    '#submit' => $submit + array('open_badge_edit_form_submit'),
  );
  
  if (!empty($openBadge->name)) { //need more logic here for issued/awarded
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete open badge'),
      '#suffix' => l(t('Cancel'), 'admin/content/open-badge-it/open-badge'), //change 
      '#submit' => $submit + array('open_badge_form_submit_delete'),
      '#weight' => 45,
    );
  }

  $form['#validate'][] = 'open_badge_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the open badge form
 */
function open_badge_edit_form_validate(&$form, &$form_state) {
  $openBadge = $form_state['open_badge'];
  
  // Notify field widgets to validate their data.
  field_attach_form_validate('open_badge', $openBadge, $form, $form_state);

}


/**
 * Form API submit callback for the open badge form.
 * 
 * @todo remove hard-coded link
 */
function open_badge_edit_form_submit(&$form, &$form_state) {
	
	
  $openBadge = entity_ui_controller('open_badge')->entityFormSubmitBuildEntity($form, $form_state);

  if ($openBadge->is_new = isset($openBadge->is_new) ? $openBadge->is_new : 0){
    $openBadge->created = time();
  }

 	$openBadge->changed = time();

  $openBadge->save();

  $form_state['redirect'] = 'admin/content/open-badge-it/open-badge';
}

/**
 * Form API submit callback for the delete button.
 * 
 * @todo Remove hard-coded path
 */
function open_badge_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/open-badge-it/open-badge/badge' . $form_state['openBadge']->open_badge_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting an open badge.
 *
 * @param $openBadge
 *   The open badge to delete
 *
 * @see confirm_form()
 */
function open_badge_delete_form($form, &$form_state, $openBadge) {
  $form_state['open_badge'] = $openBadge;

  $form['#submit'][] = 'open_badge_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete Open Badge %name?', array('%name' => $openBadge->name)),
    'admin/content/open-badge-it/open-badge/badge',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  
  return $form;
}

/**
 * Submit callback for open_badge_delete_form
 */
function open_badge_delete_form_submit($form, &$form_state) {
  $openBadge = $form_state['open_badge'];
  open_badge_delete($openBadge);

//delete files?

  drupal_set_message(t('The Open Badge %name has been deleted.', array('%name' => $openBadge->name)));
  watchdog('open_badge_it', 'Deleted Open Badge %name.', array('%name' => $openBadge->name));

  $form_state['redirect'] = 'admin/content/open-badge-it/open-badge';
}


/**
 * Page to add Open badge Entities.
 *
 * @todo Pass this through a proper theme function
 */
function open_badge_add_page() {
  $controller = entity_ui_controller('open_badge');
  return $controller->addPage();
}


/**
 * Displays the list of available open badge types for open badge creation.
 *
 * @ingroup themeable
 */
function theme_open_badge_add_list($variables) {
	
  $content = $variables['content'];

  $output = '';
  if ($content) {
    $output = '<dl class="open-badge-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer open badges')) {
      $output = '<p>' . t('Open Badges cannot be added because you have not created any Open Badge types yet. Go to the <a href="@create-open-badge-type">Open Badge Types creation page</a> to add a new Open Badge type.', array('@create-open-badge-type' => url('admin/structure/open_badge_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No Open Badge types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}

/**
 * Sets the breadcrumb for administrative open badge pages.
 */
function open_badge_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Open Badges'), 'admin/content/open-badge-it/open-badge'),
  );

  drupal_set_breadcrumb($breadcrumb);
}

