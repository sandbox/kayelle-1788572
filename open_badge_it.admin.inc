<?php

/**
 * Define the issuer settings for open_badge_it.
 *
 */

function open_badge_it_settings_edit_form($form, $form_state) {
	
	$form = system_settings_form($form);
	
	// Set default issuer api.
	
	$form['open_badge_it_api_url'] = array(
  	'#type' 					=> 'textfield', 
  	'#title' 					=> t('Issuer API url'), 
		'#default_value' 	=> variable_get('open_badge_it_api_url','http://beta.openbadges.org/issuer.js'),
  	'#size' 					=> 60, 
  	'#maxlength' 			=> 128, 
  	'#required' 			=> TRUE,
	);
	
	// Set Assertion file direcctory.
	
	$form['open_badge_it_json_dir'] = array(
  	'#type' 					=> 'textfield', 
  	'#title' 					=> t('Assertion file directory'), 
		'#default_value' 	=> variable_get('open_badge_it_json_dir'),
		'#description'		=> t('This is the subdirectory where assertion data for badges will be stored in the files directory. Do not include preceding or trailing slashes.'),
  	'#size' 					=> 60, 
  	'#maxlength' 			=> 128, 
  	'#required' 			=> TRUE,
	);
	
	$json_dir = variable_get('open_badge_it_json_dir');
	
	if (isset($json_dir)) { //ADD awarded badge count
	//	$form['open_badge_it_json_dir']['#disabled'] = TRUE;
	//	$form['open_badge_it_json_dir']['#description'] = t('This setting is disabled. At least one badge has been awarded to a user and the assertion files have been saved to this directory.');
	}
	
	// Set Open Badges images file direcctory.
	
	$form['open_badge_it_images_dir'] = array(
  	'#type' 					=> 'textfield', 
  	'#title' 					=> t('Open Badges images file directory'), 
		'#default_value' 	=> variable_get('open_badge_it_images_dir'),
		'#description'		=> t('This is the subdirectory where badge images will be stored in the files directory. Do not include preceding or trailing slashes. Once the first badge has been awarded to a user, this setting will be disabled.'),
  	'#size' 					=> 60, 
  	'#maxlength' 			=> 128, 
  	'#required' 			=> TRUE,
	);
	
	$img_dir = variable_get('open_badge_it_images_dir');
	
	if (isset($img_dir)) { //ADD awarded badge count
	//	$form['open_badge_it_images_dir']['#disabled'] = TRUE;
	//	$form['open_badge_it_images_dir']['#description'] = t('This setting is disabled. At least one badge has been awarded to a user and the badge image files has been saved to this directory.');
	}
	
	//Set default	issuer name.
	
	$site_name = variable_get('site_name');
	$form['open_badge_it_issuer_name'] = array(
  	'#type' 					=> 'textfield', 
  	'#title' 					=> t('Issuer Name'), 
		'#default_value' 	=> variable_get('open_badge_it_issuer_name',$site_name),
		'#description'		=> t('Human-readable name of the issuing agent.'),
  	'#size' 					=> 60, 
  	'#maxlength' 			=> 128, 
  	'#required' 			=> TRUE,
	);
	
	$form['open_badge_it_issuer_org'] = array(
  	'#type' 					=> 'textfield', 
  	'#title' 					=> t('Issuer Organization'), 
		'#default_value' => variable_get('open_badge_it_issuer_org',''),
		'#description'		=> t('OPTIONAL. Organization for which the badge is being issued. Example: if a scout badge is being issued, the "name" could be "Boy Scouts" and the "organization" could be "Troop #218"'),
  	'#size' 					=> 60, 
  	'#maxlength' 			=> 128, 
  	'#required' 			=> FALSE,
	);
	
	//Set default issuer contact email.
	
	$form['open_badge_it_issuer_contact_email'] = array(
  	'#type' 					=> 'textfield', 
  	'#title' 					=> t('Issuer Contact Email'), 
		'#default_value' 	=> variable_get('open_badge_it_issuer_contact_email',''),
		'#description'			=> t('OPTIONAL. A human-monitored email address associated with the issuer.'),
  	'#size' 					=> 60, 
  	'#maxlength' 			=> 128, 
  	'#required' 			=> FALSE,
	);


	$form['#submit'][] = 'open_badge_it_settings_edit_form_submit';

	return $form;

}

/**
 * Process open_badge_it_settings_edit_form submission.
 */

function open_badge_it_settings_edit_form_submit(&$form, &$form_state) {
	
		//Create JSON dir.	
	$json_directory = file_default_scheme() . '://' . $form_state['values']['open_badge_it_json_dir'];
	file_prepare_directory($json_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
	
	//Create Open Badges images dir.	
	$img_directory = file_default_scheme() . '://' . $form_state['values']['open_badge_it_images_dir'];
	file_prepare_directory($img_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
}
