<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */


class open_badge_handler_edit_link_field extends open_badge_it_handler_link_field {
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $open_badge_id = $values->{$this->aliases['open_badge_id']};
    
    return l($text, 'admin/content/open-badge-it/open-badge/badge/' . $open_badge_id . '/edit');
  }
}
