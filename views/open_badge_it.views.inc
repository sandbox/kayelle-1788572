<?php

/**
 * @file
 * Providing extra functionality for the Open Badge-It Open Badges views.
 */


/**
 * Implements hook_views_data()
 */
function open_badge_it_views_data_alter(&$data) { 
  $data['open_badge']['link_badge'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the badge.'),
      'handler' => 'open_badge_handler_link_field',
    ),
  );

  $data['open_badge']['edit_badge'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the badge.'),
      'handler' => 'open_badge_handler_edit_link_field',
    ),
  );
  $data['open_badge']['delete_badge'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the badge.'),
      'handler' => 'open_badge_handler_delete_link_field',
    ),
  );

  $data['open_badge']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this badge.'),
      'handler' => 'open_badge_handler_operations_field',
    ),
  );

	$data['open_badge_type']['table']['group']  = t('Open Badge Types');
	
	  $data['open_badge_type']['table']['join']['open_badge'] = array(
    'left_field' => 'type',
    'field' => 'type',
  );
	
	$data['open_badge_award']['table']['group']  = t('Open Badge Awards');
	
	$data['open_badge_award']['table']['join']['users'] = array(
		'left_field' => 'uid',
    'field' => 'uid',
  );

	  $data['open_badge_award']['table']['join']['open_badge'] = array(
    'left_field' => 'open_badge_id',
    'field' => 'open_badge_id',
  );

}


/**
 * Implements hook_views_default_views().
 */
function open_badge_it_views_default_views() {
  $views = array();
	
  $view = new view;
  $view->name = 'open_badge_admin';
  $view->description = 'List of Open Badges for Administration';
  $view->tag = 'open_badges_admin';
  $view->base_table = 'open_badge';
  $view->human_name = 'Open Badges Administration';
  $view->core = 7;
	$view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Open Badges';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'create any open badge type';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'name' => 'name',
  'open_badge_id' => 'open_badge_id',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'open_badge_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'Empty ';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'No open badges have been created yet';
/* Field: Open Badge: Link */
$handler->display->display_options['fields']['link_badge']['id'] = 'link_badge';
$handler->display->display_options['fields']['link_badge']['table'] = 'open_badge';
$handler->display->display_options['fields']['link_badge']['field'] = 'link_badge';
$handler->display->display_options['fields']['link_badge']['label'] = 'Open Badge';
$handler->display->display_options['fields']['link_badge']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['external'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['link_badge']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['link_badge']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['trim'] = 0;
$handler->display->display_options['fields']['link_badge']['alter']['html'] = 0;
$handler->display->display_options['fields']['link_badge']['element_label_colon'] = 1;
$handler->display->display_options['fields']['link_badge']['element_default_classes'] = 1;
$handler->display->display_options['fields']['link_badge']['hide_empty'] = 0;
$handler->display->display_options['fields']['link_badge']['empty_zero'] = 0;
/* Field: Open Badge Types: Label */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'open_badge_type';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['label']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['label']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['label']['alter']['external'] = 0;
$handler->display->display_options['fields']['label']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['label']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['label']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['label']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['label']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['label']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['label']['alter']['trim'] = 0;
$handler->display->display_options['fields']['label']['alter']['html'] = 0;
$handler->display->display_options['fields']['label']['element_label_colon'] = 1;
$handler->display->display_options['fields']['label']['element_default_classes'] = 1;
$handler->display->display_options['fields']['label']['hide_empty'] = 0;
$handler->display->display_options['fields']['label']['empty_zero'] = 0;
$handler->display->display_options['fields']['label']['hide_alter_empty'] = 1;


/* Field: Open Badge: Operations links */
$handler->display->display_options['fields']['operations']['id'] = 'operations';
$handler->display->display_options['fields']['operations']['table'] = 'open_badge';
$handler->display->display_options['fields']['operations']['field'] = 'operations';
$handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['operations']['alter']['external'] = 0;
$handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
$handler->display->display_options['fields']['operations']['alter']['html'] = 0;
$handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
$handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
$handler->display->display_options['fields']['operations']['hide_empty'] = 0;
$handler->display->display_options['fields']['operations']['empty_zero'] = 0;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'open_badge_admin_page');
$handler->display->display_options['path'] = 'admin/content/open-badge-it/open-badge/list';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'List';
$handler->display->display_options['menu']['weight'] = '-10';
$handler->display->display_options['tab_options']['type'] = 'tab';
$handler->display->display_options['tab_options']['title'] = 'Open Badge';
$handler->display->display_options['tab_options']['description'] = 'Manage Open Badges';
$handler->display->display_options['tab_options']['weight'] = '0';
$handler->display->display_options['tab_options']['name'] = 'management';

$views[$view->name] = $view;


/**
* This is the view that creates a block to put on your user's profile page to display the user's awarded badges. 
* It has a contextual argument of user id
* IMPORTANT NOTE: THIS VIEW IS JUST INCLUDED AS AN EXAMPLE. 
* - THE FIELD HANDLER FOR THE BADGE IMAGE IS BASED ON THE OPEN BADGE TYPE WHICH WILL BE DIFFERENT FOR YOU. 
* - IT ASSUMES THE URL LOCATION OF THE UID
* - IT ASSUMES ONLY ONE OPEN BADGE TYPE
**/


$view = new view;
$view->name = 'open_badge_user_awards';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'open_badge';
$view->human_name = 'Open Badge User Awards';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Open Badge Awards';
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Open Badge Types: Issuable */
$handler->display->display_options['fields']['issuable']['id'] = 'issuable';
$handler->display->display_options['fields']['issuable']['table'] = 'open_badge_type';
$handler->display->display_options['fields']['issuable']['field'] = 'issuable';
$handler->display->display_options['fields']['issuable']['label'] = '';
$handler->display->display_options['fields']['issuable']['exclude'] = TRUE;
$handler->display->display_options['fields']['issuable']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['external'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['issuable']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['issuable']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['trim'] = 0;
$handler->display->display_options['fields']['issuable']['alter']['html'] = 0;
$handler->display->display_options['fields']['issuable']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['issuable']['element_default_classes'] = 1;
$handler->display->display_options['fields']['issuable']['hide_empty'] = 0;
$handler->display->display_options['fields']['issuable']['empty_zero'] = 0;
$handler->display->display_options['fields']['issuable']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['issuable']['format_plural'] = 0;
/* Field: Open Badge: Open badge ID */
$handler->display->display_options['fields']['open_badge_id']['id'] = 'open_badge_id';
$handler->display->display_options['fields']['open_badge_id']['table'] = 'open_badge';
$handler->display->display_options['fields']['open_badge_id']['field'] = 'open_badge_id';
$handler->display->display_options['fields']['open_badge_id']['label'] = '';
$handler->display->display_options['fields']['open_badge_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['open_badge_id']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['external'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['open_badge_id']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['open_badge_id']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['trim'] = 0;
$handler->display->display_options['fields']['open_badge_id']['alter']['html'] = 0;
$handler->display->display_options['fields']['open_badge_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['open_badge_id']['element_default_classes'] = 1;
$handler->display->display_options['fields']['open_badge_id']['hide_empty'] = 0;
$handler->display->display_options['fields']['open_badge_id']['empty_zero'] = 0;
$handler->display->display_options['fields']['open_badge_id']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['open_badge_id']['format_plural'] = 0;
/* Field: Open Badge Types: Machine-readable name */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'open_badge_type';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = '';
$handler->display->display_options['fields']['type']['exclude'] = TRUE;
$handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['type']['alter']['external'] = 0;
$handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['type']['alter']['trim'] = 0;
$handler->display->display_options['fields']['type']['alter']['html'] = 0;
$handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['type']['element_default_classes'] = 1;
$handler->display->display_options['fields']['type']['hide_empty'] = 0;
$handler->display->display_options['fields']['type']['empty_zero'] = 0;
$handler->display->display_options['fields']['type']['hide_alter_empty'] = 1;
/* Field: Open Badge: Badge Image */
$handler->display->display_options['fields']['field_elo_image']['id'] = 'field_elo_image';
$handler->display->display_options['fields']['field_elo_image']['table'] = 'field_data_field_elo_image';
$handler->display->display_options['fields']['field_elo_image']['field'] = 'field_elo_image';
$handler->display->display_options['fields']['field_elo_image']['label'] = '';
$handler->display->display_options['fields']['field_elo_image']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_elo_image']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_elo_image']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_elo_image']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_elo_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_elo_image']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_elo_image']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_elo_image']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_elo_image']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_elo_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_elo_image']['settings'] = array(
  'image_style' => '',
  'image_link' => 'content',
);
$handler->display->display_options['fields']['field_elo_image']['field_api_classes'] = 0;
/* Field: Open Badge Awards: Status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'open_badge_award';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['label'] = '';
$handler->display->display_options['fields']['status']['exclude'] = TRUE;
$handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['status']['alter']['external'] = 0;
$handler->display->display_options['fields']['status']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim'] = 0;
$handler->display->display_options['fields']['status']['alter']['html'] = 0;
$handler->display->display_options['fields']['status']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['status']['element_default_classes'] = 1;
$handler->display->display_options['fields']['status']['hide_empty'] = 0;
$handler->display->display_options['fields']['status']['empty_zero'] = 0;
$handler->display->display_options['fields']['status']['hide_alter_empty'] = 1;
/* Field: Open Badge Awards: Open badge award ID */
$handler->display->display_options['fields']['obiuid']['id'] = 'obiuid';
$handler->display->display_options['fields']['obiuid']['table'] = 'open_badge_award';
$handler->display->display_options['fields']['obiuid']['field'] = 'obiuid';
$handler->display->display_options['fields']['obiuid']['exclude'] = TRUE;
$handler->display->display_options['fields']['obiuid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['external'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['obiuid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['obiuid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['obiuid']['alter']['html'] = 0;
$handler->display->display_options['fields']['obiuid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['obiuid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['obiuid']['hide_empty'] = 0;
$handler->display->display_options['fields']['obiuid']['empty_zero'] = 0;
$handler->display->display_options['fields']['obiuid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['obiuid']['format_plural'] = 0;
/* Field: Open Badge Awards: Uid */
$handler->display->display_options['fields']['uid']['id'] = 'uid';
$handler->display->display_options['fields']['uid']['table'] = 'open_badge_award';
$handler->display->display_options['fields']['uid']['field'] = 'uid';
$handler->display->display_options['fields']['uid']['label'] = '';
$handler->display->display_options['fields']['uid']['exclude'] = TRUE;
$handler->display->display_options['fields']['uid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['uid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['uid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['uid']['alter']['external'] = 0;
$handler->display->display_options['fields']['uid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['uid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['uid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['uid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['uid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['uid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['uid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['uid']['alter']['html'] = 0;
$handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['uid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['uid']['hide_empty'] = 0;
$handler->display->display_options['fields']['uid']['empty_zero'] = 0;
$handler->display->display_options['fields']['uid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['uid']['format_plural'] = 0;
/* Field: Global: PHP */
$handler->display->display_options['fields']['php']['id'] = 'php';
$handler->display->display_options['fields']['php']['table'] = 'views';
$handler->display->display_options['fields']['php']['field'] = 'php';
$handler->display->display_options['fields']['php']['label'] = '';
$handler->display->display_options['fields']['php']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['php']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['php']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['php']['alter']['external'] = 0;
$handler->display->display_options['fields']['php']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['php']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['php']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['php']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['php']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['php']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['php']['alter']['trim'] = 0;
$handler->display->display_options['fields']['php']['alter']['html'] = 0;
$handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['php']['element_default_classes'] = 1;
$handler->display->display_options['fields']['php']['hide_empty'] = 0;
$handler->display->display_options['fields']['php']['empty_zero'] = 0;
$handler->display->display_options['fields']['php']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php']['php_output'] = '<?php
global $user;

if ( $user->uid == $row->uid ) {
if ( $row->status == "awarded" &&  $row->issuable == 1 ) {

global $base_root;
global $base_path;

$obiuid = $row->obiuid;

drupal_add_js(array(\'baseRoot\' => $base_root, \'base_path\' =>$base_path,\'assertion_dir\'=>variable_get(\'open_badge_it_json_dir\')), \'setting\');

print \'<div class="backpack-button" id="\'.$obiuid.\'">Send to your Open Badge Backpack</a>\';

}
else {

print \'<div class="view-backpack"><a href="http://beta.openbadges.org/">View this badge in your backpack</a></div>\';

}
}
?>';
$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
/* Sort criterion: Open Badge: Name */
$handler->display->display_options['sorts']['name']['id'] = 'name';
$handler->display->display_options['sorts']['name']['table'] = 'open_badge';
$handler->display->display_options['sorts']['name']['field'] = 'name';
/* Contextual filter: Open Badge Awards: Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'open_badge_award';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
$handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
$handler->display->display_options['arguments']['uid']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['uid']['specify_validation'] = 1;
$handler->display->display_options['arguments']['uid']['validate']['type'] = 'user';
$handler->display->display_options['arguments']['uid']['validate_options']['type'] = 'either';
$handler->display->display_options['arguments']['uid']['validate_options']['restrict_roles'] = 0;
$handler->display->display_options['arguments']['uid']['validate']['fail'] = 'empty';
$handler->display->display_options['arguments']['uid']['break_phrase'] = 0;
$handler->display->display_options['arguments']['uid']['not'] = 0;

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Badges';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Open Badge Awards: Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'open_badge_award';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_action'] = 'default';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'raw';
$handler->display->display_options['arguments']['uid']['default_argument_options']['index'] = '1';
$handler->display->display_options['arguments']['uid']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['uid']['validate']['type'] = 'user';
$handler->display->display_options['arguments']['uid']['validate_options']['type'] = 'either';
$handler->display->display_options['arguments']['uid']['validate_options']['restrict_roles'] = 0;
$handler->display->display_options['arguments']['uid']['validate']['fail'] = 'empty';
$handler->display->display_options['arguments']['uid']['break_phrase'] = 0;
$handler->display->display_options['arguments']['uid']['not'] = 0;
$handler->display->display_options['block_description'] = 'Open Badge Awards';

$views[$view->name] = $view;

  return $views;

}
