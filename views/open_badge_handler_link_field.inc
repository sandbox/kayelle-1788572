<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying links to entities
 * as fields.
 */

class open_badge_handler_link_field extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['open_badge_id'] = 'open_badge_id';
		$this->additional_fields['name'] = 'name';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('view');
    $open_badge_id = $values->{$this->aliases['open_badge_id']};
		$name = $values->{$this->aliases['name']};

    return l($name, 'open-badge/' . $open_badge_id);
  }
}
