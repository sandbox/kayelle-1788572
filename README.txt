********************************************************************
                     OPEN BADGE-IT    M O D U L E
********************************************************************
Name: Open Badge-It Module
Author: Codery - Kerri Lemoie <kerri at gocodery dot com>
Drupal: 7
********************************************************************
DESCRIPTION:

This module allows you to create badges, award them to users and allow 
users to issue their badges to an obi compliant backpack like Mozilla's 
Open Badges Infrastructure.

The Open Badge entities are fieldable. They are created with the minimum 
fields necessary to create the valid assertion for the obi. However you 
can add additional fields. A template file is used to display the badge 
entity page and this page is used as the criteria url for the badge.

Once a badge is awarded the assertion data is collected into a json file.
Which is what is used to issue the badge to the obi.

This is a Drupal 7 module only and will not be backported.

Created with support from Providence After School Alliance and as part 
of the Digital Medial Learning Competition.

********************************************************************
INSTALLATION:

Once the module is enabled, go to configuration settings:
Configuration->User Interface->Open Badge-It
(admin/config/user-interface/open-badge-it/settings).

* Issuer API url: This is the url of the obi compliant backpack issuing api. 
  Default is: http://beta.openbadges.org/issuer.js (Mozilla's Open Badges 
Infrastructure)

* Assertions file directory: this is where the JSON data for each 
awarded badge is stored. 

* Open Badges images files directory: this is where the badge images 
will be stored

* Issuer Name: Human-readable name of the issuing agent. Defaults to 
the name of your site

* Issuer Organization and Issuer contact email are not required. If entered 
they will be submitted as part of the assertion data.

********************************************************************
DEPENDENCIES:

Entity

Views

********************************************************************
USAGE

To use:

* Create Open Badge Type: go to Structure->Open Badge Types
* Create Open Badges: go to: Content->Open Badge Tab
* Award Open Badges: go to: Structure->Open Badge Types
* Go to views and see example block view: Open Badge User Awards. This view is just included as an example. 
  Try cloning it and changing the badge image field to your badge image field. See limitations below.

********************************************************************
PERMISSIONS

This module includes:

* Administer Open Badge types

* Administer Open Badges

* View Open Badge Awards 

* Edit and View permissions for the specific Open Badge Types. Important 
  to select "view" for anonymous users to see badges on the site.

********************************************************************

LIMITATIONS:

In sandbox only so a whole lot. Most importantly:

 * Only works correctly with one open badge type

 * View included open_badge_user_awards block is just an example
   especially due to the limitation of one open badge type. The field for the badge image is incorrect. 
   If you change that to your image field, it should work.
 
 * Open Badge Awards should probably be located elsewhere. People?
 
 * Permissions to be better implemented

 * Drupal coding standards not met

 * Misc etc...